namespace Helpers
{
    public static class MessageFactory
    {
        public static string FactoryIsNull() => "Factory is null!";
        public static string ElementIsNull(string parentName, string elementName) => $"Object {elementName} on {parentName} is null!";
        public static string ImageIsNull(string name) => $"Image in {name} is null";
        public static string ObjectIsNull(string parentName, string objectTypeName) => $"Object {objectTypeName} on {parentName} is null!";
        public static string DictionaryValueMiss(string parentName, string dictionaryName, string keyName) => $"Dictionary {dictionaryName} on {parentName} doesnt contain {keyName}!";
    }
}