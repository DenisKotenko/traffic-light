using System;
using UnityEngine;

namespace DatabaseImitation
{
    [Serializable]
    public class Descriptions
    {
        [SerializeField]
        private string m_None =  "Off";
        [SerializeField]
        private string m_Red =  "Red";
        [SerializeField]
        private string m_Yellow = "Yellow";
        [SerializeField]
        private string m_Green = "Green";
        [SerializeField]
        private string m_BlinkingYellow = "Blinking Yellow";
        [SerializeField]
        private string m_BlinkingGreen = "Blinking Green";
        
        public string None => m_None;
        public string Red => m_Red;
        public string Yellow => m_Yellow;
        public string Green => m_Green;
        public string BlinkingYellow => m_BlinkingYellow;
        public string BlinkingGreen => m_BlinkingGreen;

    }
}