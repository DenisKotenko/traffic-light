using System;
using TrafficLightElements.Controller;
using UnityEngine;

namespace DatabaseImitation
{
    [Serializable]
    public class TrafficLightSettings
    {
        [SerializeField]
        private float m_BlinkDuration = 1;
        public float BlinkDuration => m_BlinkDuration;
        [SerializeField]
        private TrafficLightStateType[] m_States;
        public TrafficLightStateType[] States => m_States;
       
    }
}