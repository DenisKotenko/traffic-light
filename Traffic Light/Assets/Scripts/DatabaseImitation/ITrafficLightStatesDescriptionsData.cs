namespace DatabaseImitation
{
    public interface ITrafficLightStatesDescriptionsData
    {
        Descriptions GetDescriptionsData { get; }
    }
}