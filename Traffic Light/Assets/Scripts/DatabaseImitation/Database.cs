using UnityEngine;

namespace DatabaseImitation
{
    [CreateAssetMenu(fileName = "Database", menuName = "Database")]
    public class Database : ScriptableObject, IColorsData, ITrafficLightStatesDescriptionsData, ITrafficLightSettings
    {
        [SerializeField]
        private Colors m_Colors;

        public Colors GetColorsData => m_Colors;

        [SerializeField]
        private Descriptions m_Descriptions;

        public Descriptions GetDescriptionsData => m_Descriptions;
        
        [SerializeField]
        private TrafficLightSettings m_TrafficLightSettings;

        public TrafficLightSettings GetSettings => m_TrafficLightSettings;
    }
}