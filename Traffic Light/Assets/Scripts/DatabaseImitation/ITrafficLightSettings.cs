namespace DatabaseImitation
{
    public interface ITrafficLightSettings
    {
        TrafficLightSettings GetSettings { get; }
    }
}