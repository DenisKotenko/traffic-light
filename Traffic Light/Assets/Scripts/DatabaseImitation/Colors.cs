using System;
using UnityEngine;

namespace DatabaseImitation
{
    [Serializable]
    public class Colors
    {
        [SerializeField]
        private Color red;
        public Color Red => red;
        [SerializeField]
        private Color green;
        public Color Green => green;
        [SerializeField]
        private Color yellow;
        public Color Yellow => yellow;
        [SerializeField]
        private Color grey;
        public Color Grey => grey;
    }
}