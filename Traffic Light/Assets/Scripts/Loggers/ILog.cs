namespace Loggers
{
    public interface ILog
    {
        void LogError(string msg);
        void LogMessage(string msg);
        void LogWarning(string msg);
    
    }
}