namespace Loggers
{
    public static class LogSystem
    {
        private static ILog m_Logger;

        public static void SetLogger(ILog logger)
        {
            m_Logger = logger;
        }

        static LogSystem()
        {
            m_Logger = new UnityConsoleLogger();
        } 
        public static void LogError(string msg)
        {
            m_Logger.LogError(msg);
        }

        public static void LogMessage(string msg)
        {
            m_Logger.LogMessage(msg);
        }

        public static void LogWarning(string msg)
        {
            m_Logger.LogWarning(msg);
        }
    }
}