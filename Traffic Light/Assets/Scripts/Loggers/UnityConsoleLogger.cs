using UnityEngine;

namespace Loggers
{
    public class UnityConsoleLogger:ILog
    {
        public void LogError(string msg)
        {
            Debug.LogError(msg);
        }

        public void LogMessage(string msg)
        {
            Debug.Log(msg);
        }

        public void LogWarning(string msg)
        {
            Debug.LogWarning(msg);
        }
    }
}