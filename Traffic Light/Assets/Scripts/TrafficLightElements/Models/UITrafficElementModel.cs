using Loggers;
using UnityEngine;
using UnityEngine.UI;

namespace TrafficLightElements.Models
{
    public class UITrafficElementModel
    {
   
        private Color m_ActiveColor;
        private Color m_DeactiveColor;
    
        private Color GetColor(bool active) => active ? m_ActiveColor : m_DeactiveColor;
    
        public void SwitchActive(Image image,bool active)
        {
            if (image == null)
            {
                LogSystem.LogError($"Image in {nameof(UITrafficElementModel)} is null");
                return;
            }
            image.color = GetColor(active);
        }
    
    
        public class Builder
        {
            private readonly  UITrafficElementModel m_Result;
            public static Builder Create()
            {
                var model = new UITrafficElementModel();
                return new  Builder(model);
            }

            private Builder(UITrafficElementModel model)
            {
                m_Result = model;
            }
            public Builder SetActiveColor(Color  color)
            {
                m_Result.m_ActiveColor = color;
                return this;
            }
        
            public Builder SetDeactiveColor(Color  color)
            {
                m_Result.m_DeactiveColor = color;
                return this;
            }

            public static implicit operator UITrafficElementModel(Builder b)
            {
                return b.m_Result;
            }
        }
    }
}