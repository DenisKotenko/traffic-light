using DatabaseImitation;

namespace TrafficLightElements.Models
{
    public class UITrafficElementModelFactory : IUITrafficElementModelFactory
    {
        private readonly IColorsData m_ColorsDb;
        public UITrafficElementModelFactory(IColorsData colorsDb)
        {
            m_ColorsDb = colorsDb;
        }

        public UITrafficElementModel CreateGreen()
        {
            return UITrafficElementModel
                .Builder.Create()
                .SetActiveColor(m_ColorsDb.GetColorsData.Green)
                .SetDeactiveColor(m_ColorsDb.GetColorsData.Grey);
        }
        public UITrafficElementModel CreateRed()
        {
            return UITrafficElementModel
                .Builder.Create()
                .SetActiveColor(m_ColorsDb.GetColorsData.Red)
                .SetDeactiveColor(m_ColorsDb.GetColorsData.Grey);
        }
        public UITrafficElementModel CreateYellow()
        {
            return UITrafficElementModel
                .Builder.Create()
                .SetActiveColor(m_ColorsDb.GetColorsData.Yellow)
                .SetDeactiveColor(m_ColorsDb.GetColorsData.Grey);
        }
    }
}