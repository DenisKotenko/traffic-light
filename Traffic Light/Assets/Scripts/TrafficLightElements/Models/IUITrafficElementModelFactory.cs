namespace TrafficLightElements.Models
{
    public interface IUITrafficElementModelFactory
    {
        UITrafficElementModel CreateGreen();
        UITrafficElementModel CreateRed();
        UITrafficElementModel CreateYellow();
    }
}