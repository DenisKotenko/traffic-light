using DatabaseImitation;
using Helpers;
using HUDElements;
using Loggers;
using TrafficLightElements.Controller;

namespace TrafficLightElements.Args
{
    public class TrafficLightStateArgsFactory
    {
        private readonly ITrafficLightSystemData m_SystemData;
        private readonly ITrafficLightSettings m_Settings;
        private readonly TrafficLightStatesCircleIterator m_StatesCircleIterator;

        public TrafficLightStateArgsFactory(ITrafficLightSystemData systemData, ITrafficLightSettings settings)
        {
            m_SystemData = systemData;
            m_Settings = settings;
            m_StatesCircleIterator = new TrafficLightStatesCircleIterator(m_Settings.GetSettings.States);
        }

        public TrafficLightStateArgs CreateNextStateArgs()
        {
            var args = new TrafficLightStateArgs();

            if (m_SystemData == null)
            {
                LogSystem.LogError(MessageFactory.FactoryIsNull());
                return args;
            }


            var nextTrafficLightStateType = m_StatesCircleIterator.GetNext();
            args.TrafficLightStateType = nextTrafficLightStateType;
            return args;
        }

        public TrafficLightStateArgs CreatePreviousStateArgs()
        {
            var args = new TrafficLightStateArgs();

            if (m_SystemData == null)
            {
                LogSystem.LogError(MessageFactory.FactoryIsNull());
                return args;
            }

            var previousTrafficLightStateType = m_StatesCircleIterator.GetPrevious();
            args.TrafficLightStateType = previousTrafficLightStateType;
            return args;
        }

    }
}