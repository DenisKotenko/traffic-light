using System;
using TrafficLightElements.Controller;

namespace TrafficLightElements.Args
{
    public class TrafficLightStateArgs : EventArgs
    {
        public TrafficLightStateType TrafficLightStateType;
    
        public static TrafficLightStateArgs Default => new TrafficLightStateArgs();
    
    }
}