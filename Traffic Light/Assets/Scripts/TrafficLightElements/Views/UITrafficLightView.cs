using System;
using System.Collections.Generic;
using DatabaseImitation;
using Helpers;
using Loggers;
using TrafficLightElements.Args;
using TrafficLightElements.Controller;
using TrafficLightElements.FSM;
using TrafficLightElements.Models;
using UnityEngine;

namespace TrafficLightElements.Views
{
    public class UITrafficLightView: MonoBehaviour
    {
        [SerializeField]
        private UITrafficLightElementView m_Red;
        [SerializeField]
        private UITrafficLightElementView m_Yellow;
        [SerializeField]
        private UITrafficLightElementView m_Green;
        
        private  Dictionary<TrafficLightStateType, Action> m_StateStarters;
        private UITrafficLightElementView[] All => new []
        {
            m_Red, m_Yellow, m_Green
        };

        private TrafficLightFSM m_StateMachine;
        
        private IUITrafficElementModelFactory m_ElementModelFactory;
        private ITrafficLightSystemListener m_TrafficLightSystemSystemListener;
        private ITrafficLightSettings m_Settings;
        private ITrafficLightSystemData m_TrafficLightSystemData;

        private void Awake()
        {
            m_StateStarters = new Dictionary<TrafficLightStateType, Action>
            {
                {TrafficLightStateType.None,StartOff},
                {TrafficLightStateType.Red,StartRed},
                {TrafficLightStateType.Yellow,StartYellow},
                {TrafficLightStateType.Green,StartGreen},
                {TrafficLightStateType.BlinkingGreen,StartBlinkingGreen},
                {TrafficLightStateType.BlinkingYellow,StartBlinkingYellow},
            };
        }
        public UITrafficLightView InjectIUITrafficElementModelFactory(IUITrafficElementModelFactory factory)
        {
            m_ElementModelFactory = factory;
            return this;
        }
        
        public UITrafficLightView InjectITrafficLightListener(ITrafficLightSystemListener systemListener)
        {
            m_TrafficLightSystemSystemListener = systemListener;
            return this;
        }
        public UITrafficLightView InjectITrafficLightSettings(ITrafficLightSettings settings)
        {
            m_Settings = settings;
            return this;
        }
        public UITrafficLightView InjectITrafficLightData(ITrafficLightSystemData systemData)
        {
            m_TrafficLightSystemData = systemData;
            return this;
        }
        public void Setup()
        {
            m_StateMachine = new TrafficLightFSM();
            SetupElements();
            Subscribe();
            StartCurrent();

        }

        private void StartCurrent()
        {
            if (m_TrafficLightSystemData == null) 
            {
                LogSystem.LogError(MessageFactory.ObjectIsNull(name,nameof(ITrafficLightSystemData)));
                return;
            }

            StartState(m_TrafficLightSystemData.CurrentStateArgs);
        }

        private void Subscribe()
        {
            m_TrafficLightSystemSystemListener.OnStateChanged += OnStateChanged;
        }
        private void UnSubscribe()
        {
            if (m_TrafficLightSystemSystemListener == null)
                return;
            m_TrafficLightSystemSystemListener.OnStateChanged -= OnStateChanged;
        }

        private void OnDestroy()
        {
            UnSubscribe();
        }

        private void OnStateChanged(TrafficLightStateArgs trafficLightStateArgs)
        {
            StartState(trafficLightStateArgs);
        }

        private void StartState(TrafficLightStateArgs trafficLightStateArgs)
        {
            if (trafficLightStateArgs == null)
            {
                LogSystem.LogError(
                    MessageFactory
                        .ObjectIsNull(name,nameof(TrafficLightStateArgs)));
                return;
            }

            StartState(trafficLightStateArgs.TrafficLightStateType);
        }

        private void StartState(TrafficLightStateType type)
        {
            if (!m_StateStarters.ContainsKey(type))
            {
                LogSystem.LogError(MessageFactory
                    .DictionaryValueMiss(name,nameof(m_StateStarters),nameof(type)));
            }
            m_StateStarters[type].Invoke();
        }

        

        private void StartRed()
        {
            m_StateMachine.StartRed(All,m_Red);
        }

        private void StartGreen()
        {
            m_StateMachine.StartGreen(All,m_Green);
        }
        private void StartYellow()
        {
            m_StateMachine.StartYellow(All,m_Yellow);
        }
        private void StartBlinkingYellow()
        {
            m_StateMachine.StartBlinkingYellow(All,m_Yellow,m_Settings.GetSettings.BlinkDuration);
        }
        private void StartBlinkingGreen()
        {
            m_StateMachine.StartBlinkingGreen(All,m_Green,m_Settings.GetSettings.BlinkDuration);
        }
        private void StartOff()
        {
            m_StateMachine.StartOff(All);
        }
        

        private void SetupElements()
        {
            SetupRed();
            SetupYellow();
            SetupGreen();
        }

        private void SetupRed()
        {
            SetupElement(m_Red, m_ElementModelFactory.CreateRed());
        }
        private void SetupYellow()
        {
            SetupElement(m_Yellow, m_ElementModelFactory.CreateYellow());
        }
        private void SetupGreen()
        {
            SetupElement(m_Green, m_ElementModelFactory.CreateGreen());
        }

        private void SetupElement(UITrafficLightElementView element,UITrafficElementModel model)
        {
            if (model == null)
            {
                LogSystem.LogError(MessageFactory.ObjectIsNull(name,nameof(UITrafficElementModel)));
                return;
            }

            if (element == null)
            {
                LogSystem.LogError(MessageFactory.ElementIsNull(name,nameof(UITrafficLightElementView)));
                return;
            }
            element.Setup(model);
        }
    }
}