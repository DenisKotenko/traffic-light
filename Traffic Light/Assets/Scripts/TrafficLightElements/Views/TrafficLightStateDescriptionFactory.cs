using System;
using System.Collections.Generic;
using DatabaseImitation;
using Helpers;
using Loggers;
using TrafficLightElements.Controller;

namespace TrafficLightElements.Views
{
    public class TrafficLightStateDescriptionFactory
    {
       
        private readonly ITrafficLightStatesDescriptionsData m_DescriptionsData;
        private readonly ITrafficLightSystemData m_TrafficLightSystemData;
        

        private readonly Dictionary<TrafficLightStateType, Func<string>> m_DescriptionResolvers;
     
        
        public TrafficLightStateDescriptionFactory(ITrafficLightStatesDescriptionsData descriptionsData, ITrafficLightSystemData trafficLightSystemData)
        {
            m_DescriptionsData = descriptionsData;
            m_TrafficLightSystemData = trafficLightSystemData;

            m_DescriptionResolvers = new Dictionary<TrafficLightStateType, Func<string>>
            {
                {TrafficLightStateType.None,GetNoneDescription},
                {TrafficLightStateType.Red,GetRedDescription},
                {TrafficLightStateType.Green,GetGreenDescription},
                {TrafficLightStateType.Yellow,GetYellowDescription},
                {TrafficLightStateType.BlinkingYellow,GetBlinkingYellowDescription},
                {TrafficLightStateType.BlinkingGreen,GetBlinkingGreenDescription},
              
            };
            
        }

        public DescriptionArgs GetArgs(TrafficLightStateType stateType)
        {
            return new DescriptionArgs
            {
                Msg = GetDescription(stateType)
            };
        }

        public DescriptionArgs GetCurrent()
        {
            return GetArgs(m_TrafficLightSystemData.CurrentStateArgs.TrafficLightStateType);
        }

      
        private string GetDescription(TrafficLightStateType stateType)
        {
            if (m_DescriptionsData == null)
            {
                LogSystem
                    .LogError(MessageFactory
                    .ObjectIsNull(nameof(TrafficLightStateDescriptionFactory),
                        nameof(ITrafficLightStatesDescriptionsData)));
                return string.Empty;
            }

            if (!m_DescriptionResolvers.ContainsKey(stateType))
            {
                LogSystem
                    .LogError(MessageFactory
                        .DictionaryValueMiss(
                            nameof(TrafficLightStateDescriptionFactory),
                            nameof(m_DescriptionResolvers),
                            nameof(stateType)));
                return string.Empty;
                
            }

            return m_DescriptionResolvers[stateType].Invoke();


        }

        private string GetRedDescription()
        {
            return m_DescriptionsData.GetDescriptionsData.Red;
        }
        private string GetYellowDescription()
        {
            return m_DescriptionsData.GetDescriptionsData.Yellow;
        }
        private string GetGreenDescription()
        {
            return m_DescriptionsData.GetDescriptionsData.Green;
            
        }
        private string GetBlinkingYellowDescription()
        {
            return m_DescriptionsData.GetDescriptionsData.BlinkingYellow;
        }
        private string GetBlinkingGreenDescription()
        {
            return m_DescriptionsData.GetDescriptionsData.BlinkingGreen;
        }
        private string GetNoneDescription()
        {
            return m_DescriptionsData.GetDescriptionsData.None;
        }

    }
}