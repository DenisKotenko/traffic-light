namespace TrafficLightElements.Views
{
    public interface ITrafficLightElement
    {
        void SwitchActive(bool active);
    }
}