using Helpers;
using Loggers;
using TMPro;
using TrafficLightElements.Args;
using TrafficLightElements.Controller;
using UnityEngine;

namespace TrafficLightElements.Views
{
    public class TrafficLightStateDescriptionView : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI m_StateDescriptionText;

        private ITrafficLightSystemListener m_TrafficLightSystemListener;
        private TrafficLightStateDescriptionFactory m_DescriptionFactory;

        public TrafficLightStateDescriptionView InjectTrafficLightListener(ITrafficLightSystemListener systemListener)
        {
            m_TrafficLightSystemListener = systemListener;
            return this;
        }

        public TrafficLightStateDescriptionView InjectTrafficLightStateDescriptionFactory(
            TrafficLightStateDescriptionFactory factory)
        {
            m_DescriptionFactory = factory;
            return this;
        }

        public void Setup()
        {
            Subscribe();
            SetDescriptionText(m_DescriptionFactory?.GetCurrent());
        }

        private void Subscribe()
        {
            if (m_TrafficLightSystemListener == null)
            {
                LogSystem.LogError(MessageFactory.ObjectIsNull(name, nameof(m_TrafficLightSystemListener)));
                return;
            }

            m_TrafficLightSystemListener.OnStateChanged += OnStateChanged;
        }

        private void UnSubscribe()
        {
            if (m_TrafficLightSystemListener == null)
                return;
            
            m_TrafficLightSystemListener.OnStateChanged -= OnStateChanged;
        }

        private void OnDestroy()
        {
            UnSubscribe();
        }

        private void OnStateChanged(TrafficLightStateArgs trafficLightStateArgs)
        {
            if (m_DescriptionFactory == null)
            {
                LogSystem.LogError(MessageFactory.ObjectIsNull(name, nameof(m_DescriptionFactory)));
                return;
            }

            var descriptionArgs = GetDescriptionArgs(trafficLightStateArgs);
            SetDescriptionText(descriptionArgs);
        }

        private DescriptionArgs GetDescriptionArgs(TrafficLightStateArgs trafficLightStateArgs)
        {
            if (trafficLightStateArgs == null)
            {
                LogSystem.LogError(MessageFactory.ObjectIsNull(name, nameof(TrafficLightStateArgs)));
                return new DescriptionArgs();
            }

            return m_DescriptionFactory.GetArgs(trafficLightStateArgs.TrafficLightStateType);
        }

        private void SetDescriptionText(DescriptionArgs args)
        {
            if (args == null)
            {
                LogSystem.LogError(MessageFactory.ObjectIsNull(name, nameof(DescriptionArgs)));
                return;
            }

            SetDescriptionText(args.Msg);
        }

        private void SetDescriptionText(string msg)
        {
            m_StateDescriptionText.text = msg;
        }
    }
}