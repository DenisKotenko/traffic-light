using Helpers;
using Loggers;
using TrafficLightElements.Models;
using UnityEngine;
using UnityEngine.UI;

namespace TrafficLightElements.Views
{
    public class UITrafficLightElementView : MonoBehaviour, ITrafficLightElement
    {
        [SerializeField]
        private Image image;
        private UITrafficElementModel m_Model;

        public void SwitchActive(bool active)
        {
            if (m_Model == null) 
            {
                LogSystem.LogError(MessageFactory.ImageIsNull(name));
                return;
            }
            m_Model.SwitchActive(image,active);
        }

        public void Setup(UITrafficElementModel model)
        {
            m_Model = model;
        }
    }
}