using System;

namespace TrafficLightElements.FSM
{
    public abstract class TrafficLightState
    {
        public abstract void Start();
        
        public event Action OnComplete = () => { };

        public void Stop()
        {
            OnStop();
            OnComplete.Invoke();
        }

        protected virtual void OnStop()
        {
            
        }
    }
}