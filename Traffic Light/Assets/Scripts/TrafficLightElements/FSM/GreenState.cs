using TrafficLightElements.Views;

namespace TrafficLightElements.FSM
{
    public class GreenState : TrafficLightState
    {
        private readonly UITrafficLightElementView[] m_Elements;
        private readonly UITrafficLightElementView m_Green;
        public GreenState(UITrafficLightElementView[] elements,UITrafficLightElementView green)
        {
            m_Green = green;
            m_Elements = elements;
        }
        public override void Start()
        {
            foreach (var element in m_Elements)
            {
                element.SwitchActive(false);
            }
            m_Green.SwitchActive(true);
        }
    }
}