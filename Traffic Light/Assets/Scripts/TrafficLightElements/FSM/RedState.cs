using System;
using TrafficLightElements.Views;

namespace TrafficLightElements.FSM
{
    public class RedState : TrafficLightState
    {
        private readonly UITrafficLightElementView[] m_Elements;
        private readonly UITrafficLightElementView m_Red;
        public RedState(UITrafficLightElementView[] elements,UITrafficLightElementView red)
        {
            m_Red = red;
            m_Elements = elements;
        }
        public override void Start()
        {
            foreach (var element in m_Elements)
            {
                element.SwitchActive(false);
            }
            m_Red.SwitchActive(true);
        }
    }
}