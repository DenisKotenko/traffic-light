using DG.Tweening;
using TrafficLightElements.Views;

namespace TrafficLightElements.FSM
{
    public class BlinkingGreenState : TrafficLightState
    {
        private readonly UITrafficLightElementView[] m_Elements;
        private readonly UITrafficLightElementView m_Green;
        private readonly float m_BlinkDuration;
        private Sequence m_Blinking;
        public BlinkingGreenState(UITrafficLightElementView[] elements,UITrafficLightElementView green, float blinkDuration)
        {
            m_Green = green;
            m_Elements = elements;
            m_BlinkDuration = blinkDuration;
        }

        public override void Start()
        {
            foreach (var element in m_Elements)
            {
                element.SwitchActive(false);
            }

            Blink(false);
        }

        protected override void OnStop()
        {
            m_Blinking.Kill();
        }

        private void Blink(bool on)
        {
            m_Green.SwitchActive(on);
            m_Blinking.Kill();
            m_Blinking = DOTween.Sequence().OnComplete(() => Blink(!on)).SetDelay(m_BlinkDuration);
            m_Blinking.Play();
        }
    }
}