using TrafficLightElements.Views;

namespace TrafficLightElements.FSM
{
    public class OffState : TrafficLightState
    {
        private readonly UITrafficLightElementView[] m_Elements;
        
        public OffState(UITrafficLightElementView[] elements )
        {
            m_Elements = elements;
           
        }
        public override void Start()
        {
            foreach (var element in m_Elements)
            {
                element.SwitchActive(false);
            }
        }
    }
}