using TrafficLightElements.Views;

namespace TrafficLightElements.FSM
{
    public class TrafficLightStatesFactory
    {

        public RedState CreateRed(UITrafficLightElementView [] elementViews, UITrafficLightElementView red)
        {
            return new RedState(elementViews,red);
        }
        public  YellowState CreateYellow(UITrafficLightElementView [] elementViews, UITrafficLightElementView yellow)
        {
            return new YellowState(elementViews,yellow);
        }
        public  GreenState CreateGreen(UITrafficLightElementView [] elementViews, UITrafficLightElementView green)
        {
            return new GreenState(elementViews,green);
        }
        public  BlinkingGreenState CreateBlinkingGreen(UITrafficLightElementView [] elementViews, UITrafficLightElementView green,float blinkDuration)
        {
            return new BlinkingGreenState(elementViews,green,blinkDuration);
        }
        public  BlinkingYellowState CreateBlinkingYellow(UITrafficLightElementView [] elementViews, UITrafficLightElementView yellow,float blinkDuration)
        {
            return new BlinkingYellowState(elementViews,yellow,blinkDuration);
        }
        public  OffState CreateOff(UITrafficLightElementView [] elementViews)
        {
            return new OffState(elementViews);
        }
    }
}