using TrafficLightElements.Views;

namespace TrafficLightElements.FSM
{
    public class TrafficLightFSM
    {
        private readonly TrafficLightStatesFactory m_Factory;
        private TrafficLightState m_Current;

        public TrafficLightFSM()
        {
            m_Factory = new TrafficLightStatesFactory();
        }
        private void StartState(TrafficLightState state)
        {
            m_Current?.Stop();
            m_Current = state;
            m_Current?.Start();
        }
        public void StartRed(UITrafficLightElementView [] elementViews, UITrafficLightElementView red)
        {
            var state = m_Factory.CreateRed(elementViews,red);
            StartState(state);
        }
        public void StartYellow(UITrafficLightElementView [] elementViews, UITrafficLightElementView yellow)
        {
            var state = m_Factory.CreateYellow(elementViews,yellow);
            StartState(state);
        }
        public void StartGreen(UITrafficLightElementView [] elementViews, UITrafficLightElementView green)
        {
            var state = m_Factory.CreateGreen(elementViews,green);
            StartState(state);
        }
        public void StartBlinkingGreen(UITrafficLightElementView [] elementViews, UITrafficLightElementView green, float blinkDuration)
        {
            var state = m_Factory.CreateBlinkingGreen(elementViews,green,blinkDuration);
            StartState(state);
        }
        public void StartBlinkingYellow(UITrafficLightElementView [] elementViews, UITrafficLightElementView yellow, float blinkDuration)
        {
            var state = m_Factory.CreateBlinkingYellow(elementViews,yellow,blinkDuration);
            StartState(state);
        }
        public void StartOff(UITrafficLightElementView [] elementViews)
        {
            var state = m_Factory.CreateOff(elementViews);
            StartState(state);
        }
    }
}