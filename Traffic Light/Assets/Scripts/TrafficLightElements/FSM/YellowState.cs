using TrafficLightElements.Views;

namespace TrafficLightElements.FSM
{
    public class YellowState : TrafficLightState
    {
        
        private readonly UITrafficLightElementView[] m_Elements;
        private readonly UITrafficLightElementView m_Yellow;
        public YellowState(UITrafficLightElementView[] elements,UITrafficLightElementView yellow)
        {
            m_Yellow = yellow;
            m_Elements = elements;
        }
        public override void Start()
        {
            foreach (var element in m_Elements)
            {
                element.SwitchActive(false);
            }
            m_Yellow.SwitchActive(true);
        }
    }
}