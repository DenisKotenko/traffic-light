using System;
using TrafficLightElements.Args;

namespace TrafficLightElements.Controller
{
    public interface ITrafficLightSystemListener
    {
        event Action<TrafficLightStateArgs> OnStateChanged;
    }
}