namespace TrafficLightElements.Controller
{
    public enum TrafficLightStateType
    {
        None,Red,Green,BlinkingGreen,Yellow,BlinkingYellow
    }
}