using DatabaseImitation;
using TrafficLightElements.Args;

namespace TrafficLightElements.Controller
{
    public class TrafficLightSystemFactory : ITrafficLightSystemFactory
    {
        private readonly ITrafficLightSettings m_Settings;
        public TrafficLightSystemFactory(ITrafficLightSettings settings)
        {
            m_Settings = settings;
        }
        public TrafficLightSystemController CreateTrafficLightSystemController()
        {
            return new TrafficLightSystemController(
                new TrafficLightStateArgs
                {
                    TrafficLightStateType = m_Settings.GetSettings.States[0]
                });
        }
    }
}