using TrafficLightElements.Args;

namespace TrafficLightElements.Controller
{
    public interface ITrafficLightSystemCommand
    {
        void SetState(TrafficLightStateArgs args);
    }
}