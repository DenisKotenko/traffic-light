namespace TrafficLightElements.Controller
{
    public interface ITrafficLightSystemFactory
    {
        TrafficLightSystemController CreateTrafficLightSystemController();
    }
}