using System;
using TrafficLightElements.Args;

namespace TrafficLightElements.Controller
{
    public class TrafficLightSystemController : ITrafficLightSystemCommand, ITrafficLightSystemListener, ITrafficLightSystemData
    {
        public TrafficLightStateArgs CurrentStateArgs { get; private set; } = TrafficLightStateArgs.Default;
        public event Action<TrafficLightStateArgs> OnStateChanged = args => { };

        

        public TrafficLightSystemController(TrafficLightStateArgs initial)
        {
            SetState(initial);

        }
        public void SetState(TrafficLightStateArgs args)
        {
            CurrentStateArgs = args;
            OnStateChanged.Invoke(args);
        }

    
    }
}