using TrafficLightElements.Args;

namespace TrafficLightElements.Controller
{
    public interface ITrafficLightSystemData
    {
        TrafficLightStateArgs CurrentStateArgs { get; }
        
    }
}