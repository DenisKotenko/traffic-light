using DatabaseImitation;
using HUDElements;
using Loggers;
using TrafficLightElements.Args;
using TrafficLightElements.Controller;
using TrafficLightElements.Models;
using TrafficLightElements.Views;
using UnityEngine;

namespace Root
{
    public class Root : MonoBehaviour
    {
        [SerializeField]
        private UITrafficLightView m_UITrafficLightView;

        [SerializeField]
        private TrafficLightStateDescriptionView m_DescriptionView;

        [SerializeField]
        private UIHUDView m_UIHudView;

        [SerializeField]
        private Database m_Database;

        private ITrafficLightSystemFactory m_TrafficLightSystemFactory;
        private IUITrafficElementModelFactory m_UITrafficLightElementModelFactory;
        private TrafficLightStateArgsFactory m_TrafficLightStateArgsFactory;
        private TrafficLightStateDescriptionFactory m_DescriptionFactory;


        private TrafficLightSystemController m_TrafficLightSystemController;

        private ILog m_Logger;


        private void Start()
        {
            m_Logger = new UnityConsoleLogger();
            LogSystem.SetLogger(m_Logger);

            m_TrafficLightSystemFactory = new TrafficLightSystemFactory(m_Database);

            m_TrafficLightSystemController = m_TrafficLightSystemFactory.CreateTrafficLightSystemController();

            m_UITrafficLightElementModelFactory = new UITrafficElementModelFactory(m_Database);
            m_TrafficLightStateArgsFactory = new TrafficLightStateArgsFactory(m_TrafficLightSystemController,m_Database);
            m_DescriptionFactory = new TrafficLightStateDescriptionFactory(m_Database, m_TrafficLightSystemController);


            m_UITrafficLightView
                .InjectITrafficLightListener(m_TrafficLightSystemController)
                .InjectIUITrafficElementModelFactory(m_UITrafficLightElementModelFactory)
                .InjectITrafficLightSettings(m_Database)
                .InjectITrafficLightData(m_TrafficLightSystemController)
                .Setup();

            m_UIHudView
                .InjectTrafficLightCommand(m_TrafficLightSystemController)
                .InjectTrafficLightStateArgsFactory(m_TrafficLightStateArgsFactory)
                .Setup();

            m_DescriptionView
                .InjectTrafficLightListener(m_TrafficLightSystemController)
                .InjectTrafficLightStateDescriptionFactory(m_DescriptionFactory)
                .Setup();
        }
    }
}