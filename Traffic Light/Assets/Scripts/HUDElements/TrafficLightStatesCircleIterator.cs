using TrafficLightElements.Controller;

namespace HUDElements
{
    public class TrafficLightStatesCircleIterator
    {
        private readonly TrafficLightStateType[] m_All;
        private int m_Current;

        public TrafficLightStatesCircleIterator(TrafficLightStateType[] all, int start = 0)
        {
            m_All = all;
            m_Current = start;
        }

        public TrafficLightStateType GetNext()
        {
            m_Current = NextIndex;
            return m_All[m_Current];
        }
        public TrafficLightStateType GetPrevious()
        {
            m_Current = PreviousIndex;
            return m_All[m_Current];
        }

        private int NextIndex => m_Current >= m_All.Length - 1 ? 0 : m_Current + 1;
        
        private int PreviousIndex => m_Current <= 0  ? m_All.Length -1 : m_Current - 1;
        
    }
}