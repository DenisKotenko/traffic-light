using System;
using DatabaseImitation;
using TrafficLightElements.Args;
using TrafficLightElements.Controller;
using UnityEngine;
using UnityEngine.UI;

namespace HUDElements
{
    public class UIHUDView : MonoBehaviour
    {
        [SerializeField]
        private UITrafficLightStateSwitchView m_StatesSwitch;

        private ITrafficLightSystemCommand m_TrafficLightSystemCommands;
        private TrafficLightStateArgsFactory m_ArgsFactory;

        public UIHUDView InjectTrafficLightCommand(ITrafficLightSystemCommand systemCommands)
        {
            m_TrafficLightSystemCommands = systemCommands;
            return this;
        }

        public UIHUDView InjectTrafficLightStateArgsFactory(TrafficLightStateArgsFactory factory)
        {
            m_ArgsFactory = factory;
            return this;
        }

        public void Setup()
        {
            m_StatesSwitch
                .InjectTrafficLightCommand(m_TrafficLightSystemCommands)
                .InjectTrafficLightStateArgsFactory(m_ArgsFactory);
        }
    }

    
}