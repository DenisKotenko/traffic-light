using TrafficLightElements.Args;
using TrafficLightElements.Controller;
using UnityEngine;
using UnityEngine.UI;

namespace HUDElements
{
    public class UITrafficLightStateSwitchView:MonoBehaviour
    {
        [SerializeField]
        private Button m_NextButton;
        [SerializeField]
        private Button m_PreviousButton;
    
        private ITrafficLightSystemCommand m_TrafficLightSystemCommands;
        private TrafficLightStateArgsFactory m_ArgsFactory;
    
        public UITrafficLightStateSwitchView InjectTrafficLightCommand(ITrafficLightSystemCommand systemCommands)
        {
            m_TrafficLightSystemCommands = systemCommands;
            return this;
        }
        public UITrafficLightStateSwitchView InjectTrafficLightStateArgsFactory(TrafficLightStateArgsFactory factory)
        {
            m_ArgsFactory = factory;
            return this;
        }
    

        private void Start()
        {
            m_NextButton.onClick.AddListener(OnNextClick);
            m_PreviousButton.onClick.AddListener(OnPreviousClick);
        }

        private void OnPreviousClick()
        {
            var args = m_ArgsFactory?.CreatePreviousStateArgs();
            m_TrafficLightSystemCommands?.SetState(args);
        }
    

        private void OnNextClick()
        {
            var args = m_ArgsFactory?.CreateNextStateArgs();
            m_TrafficLightSystemCommands?.SetState(args);
        }
    }
}